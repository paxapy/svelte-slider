(function (factory) {
    typeof define === 'function' && define.amd ? define(factory) :
    factory();
}((function () { 'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if ($$scope.dirty === undefined) {
                return lets;
            }
            if (typeof lets === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if ($$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    /* src/Slider.svelte generated by Svelte v3.21.0 */

    const { document: document_1 } = globals;

    function add_css() {
    	var style = element("style");
    	style.id = "svelte-1f33bmk-style";
    	style.textContent = "html{font-size:62.5%}body{font-size:1.4rem}.slider .slides .hidden{display:none}button.svelte-1f33bmk.svelte-1f33bmk{background:transparent;color:#FFF;border-color:transparent;width:3.2rem;height:3.2rem}button.svelte-1f33bmk.svelte-1f33bmk:hover,button.svelte-1f33bmk.svelte-1f33bmk:focus{background:rgba(0,0,0,0.5)}.dots.svelte-1f33bmk.svelte-1f33bmk{display:flex;align-items:center;justify-content:center;margin-top:8px}.dot.svelte-1f33bmk.svelte-1f33bmk{width:8px;height:8px;background:#000;border-radius:100%;font-size:0;margin:0.3rem;opacity:0.3}.dot.selected.svelte-1f33bmk.svelte-1f33bmk{opacity:1}.slider.svelte-1f33bmk.svelte-1f33bmk{width:80%;margin:0 auto;padding:0 0 56.25%;position:relative}.slides.svelte-1f33bmk.svelte-1f33bmk{height:100%;width:100%;display:flex;position:absolute}.controls.svelte-1f33bmk button.svelte-1f33bmk:first-child{position:absolute;left:0;top:calc(50% - 1.2rem)}.controls.svelte-1f33bmk button.svelte-1f33bmk:last-child{position:absolute;right:0;top:calc(50% - 1.2rem)}.controls.svelte-1f33bmk.svelte-1f33bmk{text-align:center;width:100%;display:block}";
    	append(document_1.head, style);
    }

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	child_ctx[15] = i;
    	return child_ctx;
    }

    // (77:1) {#each slides as slide, i}
    function create_each_block(ctx) {
    	let button;
    	let t_value = /*i*/ ctx[15] + 1 + "";
    	let t;
    	let dispose;

    	function click_handler_2(...args) {
    		return /*click_handler_2*/ ctx[13](/*i*/ ctx[15], ...args);
    	}

    	return {
    		c() {
    			button = element("button");
    			t = text(t_value);
    			attr(button, "class", "dot svelte-1f33bmk");
    			toggle_class(button, "selected", /*cur*/ ctx[1] == /*i*/ ctx[15]);
    		},
    		m(target, anchor, remount) {
    			insert(target, button, anchor);
    			append(button, t);
    			if (remount) dispose();
    			dispose = listen(button, "click", click_handler_2);
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*cur*/ 2) {
    				toggle_class(button, "selected", /*cur*/ ctx[1] == /*i*/ ctx[15]);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(button);
    			dispose();
    		}
    	};
    }

    function create_fragment(ctx) {
    	let div2;
    	let div0;
    	let t0;
    	let div1;
    	let button0;
    	let t2;
    	let button1;
    	let t4;
    	let div3;
    	let current;
    	let dispose;
    	const default_slot_template = /*$$slots*/ ctx[10].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[9], null);
    	let each_value = /*slides*/ ctx[0];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			if (default_slot) default_slot.c();
    			t0 = space();
    			div1 = element("div");
    			button0 = element("button");
    			button0.textContent = "<";
    			t2 = space();
    			button1 = element("button");
    			button1.textContent = ">";
    			t4 = space();
    			div3 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "slides svelte-1f33bmk");
    			attr(button0, "class", "svelte-1f33bmk");
    			attr(button1, "class", "svelte-1f33bmk");
    			attr(div1, "class", "controls svelte-1f33bmk");
    			attr(div2, "class", "slider svelte-1f33bmk");
    			attr(div3, "class", "dots svelte-1f33bmk");
    		},
    		m(target, anchor, remount) {
    			insert(target, div2, anchor);
    			append(div2, div0);

    			if (default_slot) {
    				default_slot.m(div0, null);
    			}

    			append(div2, t0);
    			append(div2, div1);
    			append(div1, button0);
    			append(div1, t2);
    			append(div1, button1);
    			insert(target, t4, anchor);
    			insert(target, div3, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div3, null);
    			}

    			current = true;
    			if (remount) run_all(dispose);

    			dispose = [
    				listen(window, "keyup", /*handleShortcut*/ ctx[5]),
    				listen(button0, "click", /*click_handler*/ ctx[11]),
    				listen(button1, "click", /*click_handler_1*/ ctx[12])
    			];
    		},
    		p(ctx, [dirty]) {
    			if (default_slot) {
    				if (default_slot.p && dirty & /*$$scope*/ 512) {
    					default_slot.p(get_slot_context(default_slot_template, ctx, /*$$scope*/ ctx[9], null), get_slot_changes(default_slot_template, /*$$scope*/ ctx[9], dirty, null));
    				}
    			}

    			if (dirty & /*cur, changeSlide, slides*/ 7) {
    				each_value = /*slides*/ ctx[0];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div3, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(default_slot, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(default_slot, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div2);
    			if (default_slot) default_slot.d(detaching);
    			if (detaching) detach(t4);
    			if (detaching) detach(div3);
    			destroy_each(each_blocks, detaching);
    			run_all(dispose);
    		}
    	};
    }
    let startIndex = 0;
    const ARROW_LEFT = 37;
    const ARROW_RIGHT = 39;

    function instance($$self, $$props, $$invalidate) {
    	let slides = [];
    	let cur = 0;
    	const transition_args = { duration: 200 };
    	const clamp = (number, min, max) => Math.min(Math.max(number, min), max);

    	onMount(() => {
    		$$invalidate(0, slides = [].slice.call(document.querySelector(".slides").children));

    		$$invalidate(1, cur =  Math.max(0, Math.min(startIndex, slides.length)));

    		slide();
    	});

    	function slide() {
    		slides.forEach((slide, i) => {
    			if (cur == i) {
    				slide.classList.remove("hidden");
    			} else {
    				slide.classList.add("hidden");
    			}
    		});
    	}

    	function changeSlide(to) {
    		$$invalidate(1, cur = to);
    		slide();
    	}

    	function prev(e) {
    		$$invalidate(1, cur = clamp($$invalidate(1, --cur), 0, slides.length - 1));
    		slide();
    	}

    	function next(e) {
    		$$invalidate(1, cur = clamp($$invalidate(1, ++cur), 0, slides.length - 1));
    		slide();
    	}

    	function handleShortcut(e) {
    		if (e.keyCode === ARROW_LEFT) {
    			prev();
    		}

    		if (e.keyCode === ARROW_RIGHT) {
    			next();
    		}
    	}

    	let { $$slots = {}, $$scope } = $$props;
    	const click_handler = () => prev();
    	const click_handler_1 = () => next();
    	const click_handler_2 = i => changeSlide(i);

    	$$self.$set = $$props => {
    		if ("$$scope" in $$props) $$invalidate(9, $$scope = $$props.$$scope);
    	};

    	return [
    		slides,
    		cur,
    		changeSlide,
    		prev,
    		next,
    		handleShortcut,
    		transition_args,
    		clamp,
    		slide,
    		$$scope,
    		$$slots,
    		click_handler,
    		click_handler_1,
    		click_handler_2
    	];
    }

    class Slider extends SvelteComponent {
    	constructor(options) {
    		super();
    		if (!document_1.getElementById("svelte-1f33bmk-style")) add_css();
    		init(this, options, instance, create_fragment, safe_not_equal, {});
    	}
    }

    /* demo/Demo.svelte generated by Svelte v3.21.0 */

    function add_css$1() {
    	var style = element("style");
    	style.id = "svelte-1q7shz5-style";
    	style.textContent = ".demo.svelte-1q7shz5{margin:0 auto;height:230px;width:60vw}.slide.svelte-1q7shz5{flex:1 0 auto;width:100%;height:100%;background:red;align-items:center;justify-content:center;display:flex;text-align:center;font-weight:bold;font-size:2rem;color:white}";
    	append(document.head, style);
    }

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[1] = list[i];
    	child_ctx[3] = i;
    	return child_ctx;
    }

    // (3:4) {#each slides as slide, id}
    function create_each_block$1(ctx) {
    	let div;
    	let t0_value = /*slide*/ ctx[1].content + "";
    	let t0;
    	let t1;

    	return {
    		c() {
    			div = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			set_style(div, "background", /*slide*/ ctx[1].bg);
    			attr(div, "class", "slide svelte-1q7shz5");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t0);
    			append(div, t1);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (2:2) <Slider on:change="{changed}">
    function create_default_slot(ctx) {
    	let each_1_anchor;
    	let each_value = /*slides*/ ctx[0];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	return {
    		c() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*slides*/ 1) {
    				each_value = /*slides*/ ctx[0];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach(each_1_anchor);
    		}
    	};
    }

    function create_fragment$1(ctx) {
    	let div;
    	let current;

    	const slider = new Slider({
    			props: {
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			}
    		});

    	slider.$on("change", changed);

    	return {
    		c() {
    			div = element("div");
    			create_component(slider.$$.fragment);
    			attr(div, "class", "demo svelte-1q7shz5");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			mount_component(slider, div, null);
    			current = true;
    		},
    		p(ctx, [dirty]) {
    			const slider_changes = {};

    			if (dirty & /*$$scope*/ 16) {
    				slider_changes.$$scope = { dirty, ctx };
    			}

    			slider.$set(slider_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(slider.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(slider.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_component(slider);
    		}
    	};
    }

    function changed(event) {
    	console.log(event.detail.currentSlide);
    }

    function instance$1($$self) {
    	let slides = [
    		{ content: "1", bg: "blue" },
    		{ content: "2", bg: "red" },
    		{ content: "3", bg: "green" },
    		{ content: "4", bg: "orange" }
    	];

    	return [slides];
    }

    class Demo extends SvelteComponent {
    	constructor(options) {
    		super();
    		if (!document.getElementById("svelte-1q7shz5-style")) add_css$1();
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});
    	}
    }

    new Demo({ target: document.body });

})));
