import resolve from 'rollup-plugin-node-resolve'
import css from 'rollup-plugin-postcss'
import svelte from 'rollup-plugin-svelte'
import serve from 'rollup-plugin-serve'
import html from '@gen/rollup-plugin-generate-html'

import pkg from './package.json'

const dev = process.env.NODE_ENV === 'development'

const plugins = [
  resolve(),
	css(),
	svelte()
]

const output = [
	{ file: `${pkg.module}`, 'format': 'es' },
	{ file: `${pkg.main}`, 'format': 'umd', 'name': 'svelteSlider' }
]

if (dev) {
	plugins.push(
		html({
			template: 'demo/index.html',
      filename: 'index.html',
      publicPath: 'dist'
		}),
		serve({
			contentBase: 'dist',
			port: 4213
		})
	)
}

export default {
	input: dev ? './demo/demo.js' : './src/Slider.svelte',
	output,
	plugins
}